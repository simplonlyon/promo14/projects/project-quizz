# Projet Quizz

Créer une application de type quizz/QCM/questionnaire sur le thème de votre choix (en gardant toujours en tête que 
cette application pourrait être consultée par un·e employeur·se).

## Fonctionnalités obligatoire
* Affichage des questions et choix de la réponse
* Enchaînement de plusieurs questions (sans changement de page)
* Décompte des points et affichage du score (en temps réel, et/ou à la fin)
* Responsive

Ne pas hésiter à rajouter d'autres fonctionnalités une fois celles ci implémentées (timer, plusieurs réponses possibles, réponse en champ de texte, illustrations pour certaines questions, etc.)

### Compétences à mobilisées
Dans l'idéal, vous devrez utiliser des variables, conditions, tableaux, boucles et fonctions. Manipulation du DOM (querySelector / Events).
Essayer de maintenir votre code DRY.

## Réalisation

Commencer par trouver le thème de votre quizz et une liste de questions pour celui ci.

Ensuite réaliser une ou plusieurs maquettes fonctionnelles de ce à quoi ressemblera l'application.

Vous devrez rendre un projet gitlab avec un README présentant le projet et les maquettes (et à terme pourquoi pas 
une explication de votre méthodologie pour le code)

Commentez votre code avec au moins la JS doc de vos fonctions.

